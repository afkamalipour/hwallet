#!/usr/bin/python3
from bitcoin import privtopub, pubtoaddr, sha256, history, mktx
from bitcoin import b58check_to_hex, deserialize
import sys
import hwallet
import os
import struct
import requests

try:
    wallet = hwallet.HWallet()
except Exception as e:
    print(e)
    print("Failed to init device!")
    sys.exit()

license = """https://gitlab.com/nemanjan/hwallet
Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>."""

os.system('clear')
print("\n\n%s\n\n"%license)

mainMenu = "\n\nHWallet v0.1\n\n\
1) Generate keypair\n\
2) Get keypair\n\
3) Set keypair\n\
4) New Bitcoin transaction\n\
Choice: "

try:
    privkey = ''
    pubkey = ''
    address = ''
    while True:
        print(mainMenu)
        a = sys.stdin.readline().strip('\n')

        try:
            if a == '1':
                wallet.send(hwallet.BTC_KEYPAIR_GENERATE)
                if wallet.receive()[0]['type'] != hwallet.RSP_ACK:
                    print("Failed to generate keypair!")

            elif a == '2':
                wallet.send(hwallet.BTC_KEYPAIR_GET)
                rsp = wallet.receive()
                if rsp[0]['type'] == hwallet.RSP_NAK:
                    raise Exception("Received NAK")
                elif rsp[0]['type'] != hwallet.RSP_PRIVKEY or rsp[1]['type'] != hwallet.RSP_PUBKEY:
                    raise Exception("Failed to retrieve keys")
                else:
                    privkey = bytearray(rsp[0]['data'])
                    privkey.reverse()
                    pubkey = bytearray(rsp[1]['data'])
                    pubkey.reverse()
                    # print("Private key: ", privkey.hex())
                    # print("Public key: 04%s\n"%pubkey.hex())
                    address = pubtoaddr(privtopub(privkey.hex()))
                    print("Address: %s\n"%address)
                    print(history(address))

            elif a == '3':
                print("Enter private key: ")
                pkeystr = sys.stdin.readline().strip('\n')
                pkey = bytearray.fromhex(pkeystr)
                pkey.reverse()
                wallet.send(hwallet.BTC_KEYPAIR_SET, pkey)
                if wallet.receive()[0]['type'] != hwallet.RSP_ACK:
                    print("Failed to set the keypair!")

            elif a == '4':
                if address == '':
                    raise Exception("Unknown source address")
                wallet.send(hwallet.BTC_INIT_TX)
                rsp = wallet.receive()
                if rsp[0]['type'] != hwallet.RSP_ACK:
                    raise Exception("Failed to init the transaction!")
                print("Send amount:")
                amount = int(sys.stdin.readline().strip('\n'))
                print("To address:")
                dest = sys.stdin.readline().strip('\n')
                outs = [{'value': amount, 'address': dest}]
                unspents = []
                balance = 0
                for ins in history(address):
                    if 'spend' not in ins:
                        balance += ins['value']
                        unspents.append(ins)
                txfee = 10000
                if balance < (amount + txfee):
                    raise Exception("Insufficient funds!")
                outs.append({'value': balance-amount-txfee,
                             'address': address })
                tx = deserialize(mktx(unspents, outs))

                for ins in tx['ins']:
                    data = bytearray.fromhex(ins['outpoint']['hash'])
                    data += bytearray(struct.pack('<I', ins['outpoint']['index']))
                    wallet.send(hwallet.BTC_ADD_INPUT, data)
                    rsp = wallet.receive()
                    if rsp[0]['type'] != hwallet.RSP_ACK:
                        raise Exception("Failed to add tx input!")
                    print("Sending input: {}\n".format(ins))

                for outs in tx['outs']:
                    script = bytearray.fromhex(outs['script'])
                    data = bytearray(struct.pack('<QB', outs['value'], len(script)))
                    data += script
                    wallet.send(hwallet.BTC_ADD_OUTPUT, data)
                    rsp = wallet.receive()
                    if rsp[0]['type'] != hwallet.RSP_ACK:
                        raise Exception("Failed to add tx output!")
                    print("Sending output: {}\n".format(outs))

                wallet.send(hwallet.BTC_SIGN_TX)
                rsp = wallet.receive(wait=True)
                if rsp[0]['type'] != hwallet.RSP_TX:
                   raise Exception("Failed to sign the transaction!")
                signedtx = bytearray(rsp[0]['data'])
                print(signedtx.hex())
                print("Sumbit transaction? (y/n)")
                yn = sys.stdin.readline().strip('\n')
                if yn == 'y':
                    r = requests.post('https://blockchain.info/pushtx',
                                      data={'tx': signedtx.hex()})
                    print(r.text)
                    print('https://www.blockchain.com/btc/address/'+address)

        except Exception as e:
            print(e)


except KeyboardInterrupt:
    del wallet
