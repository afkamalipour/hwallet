//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "oled.h"

static OLED oled;

static uint8_t OLED_Command(uint8_t command) {
    GPIO_WriteOutput(oled.dcGpio, oled.dcPin, 0);
    SPI_WriteAsync(oled.spi, &command, 1);
    return 0;
}

static uint8_t OLED_Update() {
    GPIO_WriteOutput(oled.dcGpio, oled.dcPin, 1);
    SPI_WriteAsync(oled.spi, oled.buffer, 512);
    return 0;
}

uint8_t OLED_Init(SPIx* spi, GPIOx* dcgpio, uint8_t dcpin,
                  GPIOx* rstgpio, uint8_t rstpin) {
    if(!spi || !dcgpio || !rstgpio)
        return 1;
    oled.spi = spi;
    oled.dcGpio = dcgpio;
    oled.dcPin = dcpin;
    oled.rstGpio = rstgpio;
    oled.rstPin = rstpin;

    SPI_Init(oled.spi, SPI_MODE_1MHZ);
    GPIO_PinInit(oled.dcGpio, oled.dcPin);
    GPIO_PinInit(oled.rstGpio, oled.rstPin);

    GPIO_WriteOutput(oled.rstGpio, oled.rstPin, 1);
    for(int i=0; i< 999999; i++);
    GPIO_WriteOutput(oled.rstGpio, oled.rstPin, 0);
    for(int i=0; i< 999999; i++);
    GPIO_WriteOutput(oled.rstGpio, oled.rstPin, 1);
    for(int i=0; i< 999999; i++);

    OLED_Command(OLED_CMD_DISPLAYOFF);
    OLED_Command(OLED_CMD_DISPCLKDIV);
    OLED_Command(0xF0);
    OLED_Command(OLED_CMD_STOPSCROLL);
    OLED_Command(OLED_CMD_STARTLINE);
    OLED_Command(OLED_CMD_MULTIPLEX);
    OLED_Command(0x1F);
    OLED_Command(OLED_CMD_CHARGEPUMP);
    OLED_Command(0x14);
    OLED_Command(OLED_CMD_COMPINS);
    OLED_Command(0x2);
    OLED_Command(OLED_CMD_SEGREMAP);
    OLED_Command(OLED_CMD_COMSCANINC);
    OLED_Command(OLED_CMD_DISPOFFSET);
    OLED_Command(0x00);
    OLED_Command(OLED_CMD_CONTRAST);
    OLED_Command(0xCF);
    OLED_Command(OLED_CMD_PRECHARGE);
    OLED_Command(0xF1);
    OLED_Command(OLED_CMD_VCOM);
    OLED_Command(0x30);
    OLED_Command(OLED_CMD_ENTIREOFF);
    OLED_Command(OLED_CMD_NORMINV);
    OLED_Command(OLED_CMD_MEMADDR);
    OLED_Command(0x00);
    OLED_Command(OLED_CMD_DISPLAYON);
    OLED_Command(OLED_CMD_PAGESTART);
    OLED_Command(OLED_CMD_PAGEADDR);
    OLED_Command(0x00);
    OLED_Command(0x03);
    OLED_Command(OLED_CMD_LOWCOL);
    OLED_Command(OLED_CMD_HICOL);

    for(int i=0; i< 999999; i++);
    OLED_Clear();
    return 0;
}

uint8_t OLED_Clear() {
    for(int i = 0; i< 512; i++)
        oled.buffer[i] = 0;
    OLED_Update();
    return 0;
}

uint8_t OLED_WriteRow(uint8_t row, char* string) {
    char* tmpstr = string;
    uint32_t pixLen = 0;
    while(*tmpstr) {
        pixLen += oled_char_widths[*tmpstr - 0x20];
        tmpstr++;
    }
    if(pixLen > 128) return 1;
    uint8_t* buffptr = &oled.buffer[(row/8)*128];
    while(*string < 0x7F && *string >= 0x20) {
        const uint8_t *charptr = &oled_font[(*string - 0x20)*oled_font_bytes_per_char];
        for(uint8_t i=0; i<oled_char_widths[*string - 0x20]; i++) {
            *buffptr |= (*(charptr + i*2)) << (row%8);
            if(row/8 < 3)
                *(buffptr+128) |= (*(charptr + i*2 + 1)) << (row%8) | (*(charptr + i*2)) >> (8-row%8);
            if(row/8 < 2)
                *(buffptr+256) |= (*(charptr + i*2 + 1)) >> (8-row%8);
            buffptr++;
        }
        string++;
    }
    OLED_Update();
    return 0;
}

uint8_t OLED_ShowHomeScreen() {
    OLED_Clear();
    return OLED_WriteRow(10, "HITB Dubai 2018 DEMO");
}
