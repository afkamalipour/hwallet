//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _OLED_H_
#define _OLED_H_
#include <stdint.h>
#include "spi.h"
#include "gpio.h"

typedef struct {
    SPIx* spi;
    GPIOx* dcGpio;
    GPIOx* rstGpio;
    uint8_t dcPin;
    uint8_t rstPin;
    uint8_t buffer[640];
} OLED;

#define OLED_CMD_LOWCOL         (0x00u)
#define OLED_CMD_HICOL          (0x10u)
#define OLED_CMD_MEMADDR        (0x20u)
#define OLED_CMD_PAGEADDR       (0x22u)
#define OLED_CMD_STOPSCROLL     (0x2Eu)
#define OLED_CMD_STARTLINE      (0x40u)
#define OLED_CMD_CONTRAST       (0x81u)
#define OLED_CMD_CHARGEPUMP     (0x8Du)
#define OLED_CMD_SEGREMAP       (0xA0u)
#define OLED_CMD_ENTIREOFF      (0xA4u)
#define OLED_CMD_NORMINV        (0xA6u)
#define OLED_CMD_MULTIPLEX      (0xA8u)
#define OLED_CMD_PAGESTART      (0xB0u)
#define OLED_CMD_COMSCANINC     (0xC0u)
#define OLED_CMD_DISPOFFSET     (0xD3u)
#define OLED_CMD_DISPCLKDIV     (0xD5u)
#define OLED_CMD_PRECHARGE      (0xD9u)
#define OLED_CMD_COMPINS        (0xDAu)
#define OLED_CMD_VCOM           (0xDBu)
#define OLED_CMD_DISPLAYOFF     (0xAEu)
#define OLED_CMD_DISPLAYON      (0xAFu)

extern const uint8_t oled_font[];
extern const uint8_t oled_char_widths[];
extern const uint8_t oled_font_height;
extern const uint8_t oled_font_bytes_per_char;

uint8_t OLED_Init(SPIx* spi, GPIOx* dcgpio, uint8_t dcpin,
                  GPIOx* rstgpio, uint8_t rstpin);
uint8_t OLED_Clear();
uint8_t OLED_WriteRow(uint8_t row, char* string);
uint8_t OLED_ShowHomeScreen();

#endif
