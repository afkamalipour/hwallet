//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "uart.h"
#include "clock.h"

void UART_Init(UARTx* uart) {
    if(uart == UART_0) {
        CLOCK_Enable(CLOCK_UART0);
    } else if (uart == UART_1) {
        CLOCK_Enable(CLOCK_UART1);
    } else if (uart == UART_2) {
        CLOCK_Enable(CLOCK_UART2);
    }
    #ifndef KL82
    else if (uart == UART_3) {
        CLOCK_Enable(CLOCK_UART3);
    } else if (uart == UART_4) {
        CLOCK_Enable(CLOCK_UART4);
    }
    #endif
    else return;

    // osr = 26, sbr = 4
    uart->BAUD = (25u << 24u) | 4u;
    uart->CTRL = 0;
    uart->WATER = 0;
    // enable and flush FIFOs
    uart->FIFO |= 0x88u;
    uart->FIFO |= 0xC000u;
    // clear status bits
    uart->STAT |= 0xC01FC000u;
    // enable Rx and Tx
    uart->CTRL |= 0xC0000u;
}

uint8_t UART_WriteAsync(UARTx* uart, uint8_t* buff, uint32_t len, uint32_t timeout) {
    if(!buff) {
        return 1;
    }

    while(len--) {
        // Tx data register empty?
        uint32_t ticks = 0;
        while(!(uart->STAT & 0x800000u) && !(uart->FIFO & 0x800000u)) {
            if(timeout && (ticks++ > timeout))
                return 2;
        }
        uart->DATA = *(buff++);
    }
    return 0;
}

uint8_t UART_ReadAsync(UARTx* uart, uint8_t* buff, uint32_t len, uint32_t timeout) {
    if(!buff) {
        return 1;
    }

    while(len--) {
        // Check buffer overrun
        if(uart->STAT & 0x80000u) {
            uart->STAT |= 0x80000u;
        }
        uint32_t ticks = 0;
        while(!(uart->WATER & 0xFF000000u)) {
            if(timeout && (ticks++ > timeout))
                return 2;
        }
        *(buff++) = uart->DATA;
    }

    return 0;
}
