//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _CRYPTO_H_
#define _CRYPTO_H_
#include <stdint.h>

typedef struct {
    volatile uint32_t MODE;
    uint8_t RESERVED_0[4];
    volatile uint32_t KS;
    uint8_t RESERVED_1[4];
    volatile uint32_t DS;
    uint8_t RESERVED_2[0x1C];
    volatile uint32_t CMD;
    volatile uint32_t CTRL;
    uint8_t RESERVED_3[8];
    volatile uint32_t CW;
    uint8_t RESERVED_4[4];
    volatile uint32_t STATUS;
    volatile uint32_t ESTATUS;
    uint8_t RESERVED_5[0x30];
    volatile uint32_t A_SIZE;
    uint8_t RESERVED_6[4];
    volatile uint32_t B_SIZE;
    uint8_t RESERVED_7[4];
    volatile uint32_t N_SIZE;
    uint8_t RESERVED_8[4];
    volatile uint32_t E_SIZE;
    uint8_t RESERVED_9[0x64];
    volatile uint32_t CTX[16];
    uint8_t RESERVED_10[0x680];
    volatile uint32_t FIFOSTATUS;
    uint8_t RESERVED_11[0x1C];
    volatile uint32_t IFIFO;
    uint8_t RESERVED_12[0x1C];
    volatile uint32_t A[64];
    uint8_t RESERVED_13[0x100];
    volatile uint32_t B[64];
    uint8_t RESERVED_14[0x100];
    volatile uint32_t N[64];
    uint8_t RESERVED_15[0x100];
    volatile uint32_t E[64];
} _LTC;

#ifdef KL82
#define LTC       ((_LTC*)0x40051000u)
#else
#define LTC       ((_LTC*)0x400D1000u)
#endif

// LTC Mode register values
#define LTC_PKHA_CLEARMEM_ABNE  (0x8F0001u)
#define LTC_PKHA_MOD_ADD        (0x800002u)
#define LTC_PKHA_MOD_SUB1       (0x800003u)
#define LTC_PKHA_MOD_MUL        (0x800005u)
#define LTC_PKHA_MOD_AMODN_A    (0x800107u)
#define LTC_PKHA_MOD_AMODN_B    (0x800007u)
#define LTC_PKHA_MOD_INV        (0x800008u)
#define LTC_PKHA_ECC_MUL_TEQ    (0x80040Bu)
// page 1210
#define LTC_PKHA_COPY_NSIZE     (0x800010u)
#define LTC_PKHA_COPY_SSIZE     (0x800011u)
#define LTC_PKHA_COPY_A_N2      (0xC90u)
#define LTC_PKHA_COPY_N2_B0     (0x610u)
#define LTC_PKHA_COPY_B_E       (0x20810u)
#define LTC_PKHA_COPY_B1_A0     (0x20110u)

#ifdef KL82
#define LTC_MDHA_SHA256_INIT        (0x430004u)
#define LTC_MDHA_SHA256_UPDATE      (0x430000u)
#define LTC_MDHA_SHA256_FINALIZE    (0x430008u)
#define LTC_MDHA_SHA256_INITFIN     (0x43000Cu)
#endif

#define LTC_CW_ALL              (0xC000F06Du)
#define LTC_CTRL_SWAP_ALL       (0x00F30000u)
#define LTC_STATUS_DI           (0x00010000u)
#define LTC_STATUS_EI           (0x00100000u)
#define LTC_FIFOSTATUS_IFF      (1<<15)

typedef struct {
    uint32_t type;
    uint32_t p_size;
    uint8_t p[64];
    uint8_t a[64];
    uint8_t b[64];
    uint8_t Gx[64];
    uint8_t Gy[64];
    uint32_t n_size;
    uint8_t n[64];
} LTC_ECC_Curve;

#define ECC_CURVE_PRIME     (0x00000000u)
#define ECC_CURVE_BINARY    (0x00020000u)

typedef struct {
    volatile uint32_t MCTL;
    volatile uint32_t SCMISC;
    volatile uint32_t PKRRNG;
    volatile uint32_t PKRMAX_SQ;
    volatile uint32_t SDCTL;
    volatile uint32_t SBLIM_TOTSAM;
    volatile uint32_t FRQMIN;
    volatile uint32_t FRQMAX_CNT;
    volatile uint32_t SCMCL;
    volatile uint32_t SCRCL[6];
    volatile uint32_t STATUS;
    volatile uint32_t ENT[16];
    volatile uint32_t PKRCNT[8];
    #ifndef KL82
    uint8_t RESERVED_0[0x10];
    #endif
    volatile uint32_t SECCFG;
    volatile uint32_t INTCTRL;
    volatile uint32_t INTMASK;
    volatile uint32_t INTSTAT;
    #ifdef KL82
    uint8_t RESERVED_0[0x20];
    #else
    uint8_t RESERVED_1[0x30];
    #endif
    volatile uint32_t VID[2];
} _TRNG;

#ifdef KL82
#define TRNG    ((_TRNG*)0x40025000u)
#else
#define TRNG    ((_TRNG*)0x400A0000u)
#endif

#define TRNG_MCTL_ERR       (0x1000u)
#define TRNG_MCTL_ENTVAL    (0x400u)

#ifndef KL82
typedef struct {
    volatile uint32_t DIRECT;
} _MMCAU;

#define MMCAU   ((_MMCAU*)0xE0081000u)

typedef struct {
    volatile uint32_t CASR;
    volatile uint32_t CAA;
    volatile uint32_t CA[9];
} _MMCAU_CMD;

#define MMCAU_LDR ((_MMCAU_CMD*)0xE0081840u)
#define MMCAU_STR ((_MMCAU_CMD*)0xE0081880u)
#define MMCAU_ADR ((_MMCAU_CMD*)0xE00818C0u)

#define MMCAU_CMD_SIGMA1        (0x80000000u | (0x12Bu << 22))
#define MMCAU_CMD_SIGMA0        (0x80000000u | (0x12Au << 22))
#define MMCAU_CMD_SHIFT         (0x80000000u | (0x150u << 22))
#define MMCAU_CMD_SIGMA1_CH     (0x80100200u | (0x59u << 22) | (0x129u << 11) | 0x126u)
#define MMCAU_CMD_SIGMA0_MAJ    (0x80100200u | (0x9Au << 22) | (0x128u << 11) | 0x127u)
#endif

typedef struct {
    uint8_t r[64];
    uint8_t s[64];
} ECDSA_Signature;

#define CRYPTO_BIGNUM_SIZE      32
typedef struct {
    uint8_t num[CRYPTO_BIGNUM_SIZE];
    uint8_t len;
} Bignum;

void CRYPTO_Init();
uint8_t CRYPTO_Random(uint8_t* buff, uint32_t len);
uint8_t CRYPTO_SHA256(uint8_t* buff, uint32_t len, uint32_t* result);
uint8_t CRYPTO_ECDSA_Sign(const LTC_ECC_Curve* curve, const uint8_t* priv,
                          uint8_t* hash, uint32_t len, ECDSA_Signature* sig);
uint8_t CRYPTO_ECDSA_GetPublicKey(const LTC_ECC_Curve* curve,
                                  const uint8_t* priv, uint8_t* pub);

uint8_t CRYPTO_Bignum_Init(Bignum* num, uint8_t* buff, uint8_t len);
uint8_t CRYPTO_Bignum_Mod(Bignum* num, Bignum* mod, Bignum* res);
uint8_t CRYPTO_Bignum_Div(Bignum* num, Bignum* divisor, Bignum* res);
uint8_t CRYPTO_Bignum_Sub(Bignum* min, Bignum* sub, Bignum* res);
uint8_t CRYPTO_Bignum_IsNull(Bignum* num);

extern const LTC_ECC_Curve secp256k1;

#endif
