//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _GPIO_H_
#define _GPIO_H_
#include <stdint.h>

typedef struct {
    volatile uint32_t PDOR;
    volatile uint32_t PSOR;
    volatile uint32_t PCOR;
    volatile uint32_t PTOR;
    volatile uint32_t PDIR;
    volatile uint32_t PDDR;
} GPIOx;

#define GPIO_A      ((GPIOx*) 0x400FF000u)
#define GPIO_B      ((GPIOx*) 0x400FF040u)
#define GPIO_C      ((GPIOx*) 0x400FF080u)
#define GPIO_D      ((GPIOx*) 0x400FF0C0u)
#define GPIO_E      ((GPIOx*) 0x400FF100u)

#define GPIO_OUTPUT(x, y)       (((y & 1) << 6) | (x & 0x1F))
#define GPIO_INPUT(x)           ((1 << 7) | (x & 0x1F))

void GPIO_PinInit(GPIOx* gpio, uint8_t pin);
void GPIO_WriteOutput(GPIOx* gpio, uint8_t pin, uint8_t value);
uint8_t GPIO_ReadInput(GPIOx* gpio, uint8_t pin);

#endif
