//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _UTILS_H_
#define _UTILS_H_
#include <stdint.h>

uint8_t UTIL_IntToHex(uint8_t* num, uint8_t lenin, char* str, uint32_t lenout);
uint8_t UTIL_IntToDec(uint8_t* num, uint32_t lenin, char* str, uint32_t lenout);
uint8_t UTIL_IntToBase58(uint8_t* num, uint32_t lenin, char* str, uint32_t lenout);


#endif
