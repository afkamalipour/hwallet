//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "common/flash.h"

uint8_t FLASH_Write(uint32_t* src, uint32_t len, uint32_t dst) {
    if(FLASH_SectorErase(dst))
        return 1;
    // Previous command completed?
    while(!(FTFA->FSTAT & FTFA_FSTAT_CCIF));
    // Clear errors
    FTFA->FSTAT = FTFA_FSTAT_ACCERR | FTFA_FSTAT_FPVIOL;
    while(len > 0) {
        // Prepare flash write command
        FTFA->FCCOB[0] = 0x06000000u | (dst & 0xFFFFFFu);
        FTFA->FCCOB[1] = *src++;
        // Clear complete flag
        FTFA->FSTAT = FTFA_FSTAT_CCIF;
        // Wait to complete
        while(!(FTFA->FSTAT & FTFA_FSTAT_CCIF));
        // Check errors
        if(FTFA->FSTAT & (FTFA_FSTAT_FPVIOL | FTFA_FSTAT_ACCERR)) {
            return 1;
        }
        dst += 4;
        len -= 4;
    }
    return 0;
}

uint8_t FLASH_SectorErase(uint32_t sec) {
    sec &= ~0xFFFu;
    // Previous command completed?
    while(!(FTFA->FSTAT & FTFA_FSTAT_CCIF));
    // Clear errors
    FTFA->FSTAT = FTFA_FSTAT_ACCERR | FTFA_FSTAT_FPVIOL;
    // Prepare sector erase command
    FTFA->FCCOB[0] = 0x09000000u | (sec & 0xFFFFFFu);
    // Clear complete flag
    FTFA->FSTAT = FTFA_FSTAT_CCIF;
    // Wait to complete
    while(!(FTFA->FSTAT & FTFA_FSTAT_CCIF));
    // Check errors
    if(FTFA->FSTAT & (FTFA_FSTAT_FPVIOL | FTFA_FSTAT_ACCERR)) {
        return 1;
    }
    return 0;
}
