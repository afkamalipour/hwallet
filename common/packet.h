//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _PACKET_H_
#define _PACKET_H_
#include <stdint.h>
#include "uart.h"

#ifdef KL82
#define PACKET_UART        UART_0
#else
#define PACKET_UART        UART_4
#endif

#define PACKET_RSP_ACK             0u
#define PACKET_RSP_NAK             1u
#define PACKET_RSP_PRIVKEY         2u
#define PACKET_RSP_PUBKEY          3u
#define PACKET_RSP_SIG_R           4u
#define PACKET_RSP_SIG_S           5u
#define PACKET_RSP_TX              6u

#define PACKET_BITCOIN      1u

#define PACKET_MODULE(x)    (((x) >> 8) & 0xFFu)
#define PACKET_FUNC(x)      ((x) & 0xFFu)

#define PACKET_DATA_SIZE   32
typedef struct {
    uint16_t type;
    uint16_t length;
    uint8_t data[PACKET_DATA_SIZE];
    uint32_t crc;
} Packet;

typedef struct {
    volatile uint32_t DATA;
    volatile uint32_t GPOLY;
    volatile uint32_t CTRL;
} _CRC;

#define CRC        ((_CRC*)0x40032000u)

void PACKET_Init(void);
uint8_t PACKET_Receive(Packet* msg);
uint8_t PACKET_Send(uint16_t type, uint8_t* data, uint16_t length);

#define PACKET_LONG_ERROR       0u
#define PACKET_LONG_INCOMPLETE  1u
#define PACKET_LONG_DONE        2u

#define PACKET_LONG_MAX_SIZE    1024u

uint8_t PACKET_Long(Packet* msg);
uint8_t* PACKET_GetLong(uint32_t* len);

static inline uint8_t PACKET_SendACK() {
    return PACKET_Send(PACKET_RSP_ACK, 0, 0);
}
static inline uint8_t PACKET_SendNAK() {
    return PACKET_Send(PACKET_RSP_NAK, 0, 0);
}

#endif
