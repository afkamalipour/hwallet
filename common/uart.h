//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _UART_H_
#define _UART_H_
#include <stdint.h>

typedef struct {
    volatile uint32_t BAUD;
    volatile uint32_t STAT;
    volatile uint32_t CTRL;
    volatile uint32_t DATA;
    volatile uint32_t MATCH;
    volatile uint32_t MODIR;
    volatile uint32_t FIFO;
    volatile uint32_t WATER;
} UARTx;

#ifdef KL82
#define UART_0      ((UARTx*)0x40054000u)
#define UART_1      ((UARTx*)0x40055000u)
#define UART_2      ((UARTx*)0x40056000u)
#else
#define UART_0      ((UARTx*)0x400C4000u)
#define UART_1      ((UARTx*)0x400C5000u)
#define UART_2      ((UARTx*)0x400C6000u)
#define UART_3      ((UARTx*)0x400C7000u)
#define UART_4      ((UARTx*)0x400D6000u)
#endif

// Default: 115200, 8-bit, no parity, one stop bit
void UART_Init(UARTx* uart);

uint8_t UART_WriteAsync(UARTx* uart, uint8_t* buff, uint32_t len, uint32_t timeout);
uint8_t UART_ReadAsync(UARTx* uart, uint8_t* buff, uint32_t len, uint32_t timeout);

#endif
