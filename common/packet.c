//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "packet.h"
#include "clock.h"

static uint8_t longBuff[PACKET_LONG_MAX_SIZE];
static uint32_t longBuffLen = 0, longBuffCur = 0;
static uint16_t longType = 0;

void PACKET_Init(void) {
    UART_Init(PACKET_UART);
    CLOCK_Enable(CLOCK_CRC);
}

static uint32_t CRC32_Calculate(uint32_t* msg, uint32_t len) {
    CRC->CTRL = 0xA5000000u;
    CRC->GPOLY = 0x04C11DB7u;
    CRC->CTRL = 0xA7000000u;
    CRC->DATA = 0xFFFFFFFFu;
    CRC->CTRL = 0xA5000000u;

    for(uint32_t i=0; i<len; i++)
        CRC->DATA = msg[i];
    return CRC->DATA;
}

uint8_t PACKET_Receive(Packet* msg) {
    if(!msg) return 1;

    msg->crc = 0;
    msg->type = 0;
    msg->length = 0;
    for(int i=0; i < PACKET_DATA_SIZE; i++)
        msg->data[i] = 0;

    do {
        uint32_t magic = 0;
        do {
            UART_ReadAsync(PACKET_UART, (uint8_t*)&magic, 4, 1000000);
        } while(magic != 0xDEDABABAu);
        UART_ReadAsync(PACKET_UART, (uint8_t*)msg, sizeof(Packet), 0);
    } while(msg->crc != CRC32_Calculate((uint32_t*)msg, PACKET_DATA_SIZE/4+1));
    return 0;
}

uint8_t PACKET_Send(uint16_t type, uint8_t* data, uint16_t length){
    Packet rsp;
    rsp.type = type;

    if(data == 0)
        length = 0;
    uint16_t numPkts = length/PACKET_DATA_SIZE;
    if(length % PACKET_DATA_SIZE || length == 0)
        numPkts++;

    uint16_t dataptr = 0;
    for(uint16_t i=0; i<numPkts; i++) {
        rsp.length = length - dataptr;
        for(uint32_t j=0; j < PACKET_DATA_SIZE; j++) {
            rsp.data[j] = (dataptr < length) ? data[dataptr++] : 0;
        }

        rsp.crc = CRC32_Calculate((uint32_t*)&rsp, PACKET_DATA_SIZE/4+1);
        uint32_t rspmagic = 0xBABADEDAu;
        UART_WriteAsync(PACKET_UART, (uint8_t*)&rspmagic, 4, 0);
        UART_WriteAsync(PACKET_UART, (uint8_t*)&rsp, sizeof(Packet), 0);
    }
    return 0;
}

uint8_t PACKET_Long(Packet* msg) {
    if(longType != msg->type) {
        longBuffCur = 0;
        longBuffLen = msg->length;
        if(longBuffLen > PACKET_LONG_MAX_SIZE) {
            longBuffLen = 0;
            PACKET_SendNAK();
            return PACKET_LONG_ERROR;
        }
    }
    uint32_t bytecnt = (msg->length < PACKET_DATA_SIZE) ? msg->length : PACKET_DATA_SIZE;
    if((longBuffCur + bytecnt) > longBuffLen) {
        longType = 0;
        PACKET_SendNAK();
        return PACKET_LONG_ERROR;
    }
    for(uint32_t i=0; i < bytecnt; i++)
        longBuff[longBuffCur++] = msg->data[i];
    longType = msg->type;
    if(longBuffLen > longBuffCur) {
        PACKET_SendACK();
        return PACKET_LONG_INCOMPLETE;
    }
    longType = 0;

    return PACKET_LONG_DONE;
}

uint8_t* PACKET_GetLong(uint32_t* len) {
    *len = longBuffLen;
    return longBuff;
}
