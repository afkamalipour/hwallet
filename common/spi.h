//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#ifndef _SPI_H_
#define _SPI_H_
#include <stdint.h>

typedef struct {
    volatile uint32_t MCR;
    uint8_t RESERVED_0[4];
    volatile uint32_t TCR;
    volatile uint32_t CTAR[2];
    uint8_t RESERVED_1[24];
    volatile uint32_t SR;
    volatile uint32_t PSER;
    volatile uint32_t PUSHR;
    volatile uint32_t POPR;
    volatile uint32_t TXFR[4];
    uint8_t RESERVED_2[48];
    volatile uint32_t RXFR[4];
} SPIx;

#define SPI_0      ((SPIx*)0x4002C000u)
#define SPI_1      ((SPIx*)0x4002D000u)
#ifndef KL82
#define SPI_2      ((SPIx*)0x400AC000u)
#endif

#define SPI_MODE_CPOL_0        0
#define SPI_MODE_CPOL_1        2
#define SPI_MODE_CPHA_0        0
#define SPI_MODE_CPHA_1        1

#define SPI_MODE_500KHZ        (0<<2)
#define SPI_MODE_1MHZ          (1<<2)

// Master mode only
uint8_t SPI_Init(SPIx* spi, uint8_t mode);
uint8_t SPI_WriteAsync(SPIx* spi, uint8_t* buff, uint32_t len);

#endif
