//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include "utils.h"
#include "crypto.h"

static const char base58[] = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
static const char hexval[] = "0123456789abcdef";

uint8_t UTIL_IntToHex(uint8_t* num, uint8_t lenin, char* str, uint32_t lenout) {
    if(!num || !str || (lenout < lenin*2+1)) return 1;
    for(int i=lenin-1; i>=0; i--) {
         *(str++) = hexval[(num[i]>>4) & 0xF];
         *(str++) = hexval[num[i] & 0xF];
    }
    *str = 0;
    return 0;
}

uint8_t UTIL_IntToDec(uint8_t* num, uint32_t lenin, char* str, uint32_t lenout) {
    if(!str || !num || lenin > CRYPTO_BIGNUM_SIZE) return 1;
    Bignum in, ten;
    for(uint32_t i=0; i<CRYPTO_BIGNUM_SIZE; i++) {
            in.num[i] = (i<lenin) ? num[i] : 0;
            ten.num[i] = (i==0) ? 10 : 0;
    }
    in.len = CRYPTO_BIGNUM_SIZE;
    ten.len = 1;

    for(int i=lenout-2; i>=0; i--) {
        Bignum tmp;
        tmp.len = CRYPTO_BIGNUM_SIZE;
        CRYPTO_Bignum_Div(&in, &ten, &tmp);
        if(!CRYPTO_Bignum_IsNull(&tmp)) {
            CRYPTO_Bignum_Mod(&in, &ten, &tmp);
            str[i] = hexval[tmp.num[0]];
            CRYPTO_Bignum_Div(&in, &ten, &in);
        } else if(!CRYPTO_Bignum_IsNull(&in)) {
            str[i] = hexval[in.num[0]];
            CRYPTO_Bignum_Div(&in, &ten, &in);
        } else {
           str[i] = ' ';
        }
    }
    str[lenout-1] = 0;
    return 0;
}

uint8_t UTIL_IntToBase58(uint8_t* num, uint32_t lenin, char* str, uint32_t lenout) {
    if(!str || !num || lenin > CRYPTO_BIGNUM_SIZE) return 1;
    Bignum in, b58;
    uint8_t lead_zero = 0;
    for(int32_t i=lenin-1; i>0; i--) {
        if(num[i] == 0)
            lead_zero++;
    }
    for(uint32_t i=0; i<CRYPTO_BIGNUM_SIZE; i++) {
            in.num[i] = (i<lenin) ? num[i] : 0;
            b58.num[i] = (i==0) ? 58 : 0;
    }
    in.len = CRYPTO_BIGNUM_SIZE;
    b58.len = 1;

    for(int i=lenout-2; i>=0; i--) {
        Bignum tmp;
        tmp.len = CRYPTO_BIGNUM_SIZE;

        CRYPTO_Bignum_Div(&in, &b58, &tmp);
        if(!CRYPTO_Bignum_IsNull(&tmp)) {
            CRYPTO_Bignum_Mod(&in, &b58, &tmp);
            str[i] = base58[tmp.num[0]];
            CRYPTO_Bignum_Div(&in, &b58, &in);
        } else if(!CRYPTO_Bignum_IsNull(&in)) {
            str[i] = base58[in.num[0]];
            CRYPTO_Bignum_Div(&in, &b58, &in);
        } else {
            if(lead_zero) {
                if(str[i+1] != '1')
                    str[i] = '1';
                else
                    str[i] = ' ';
                lead_zero--;
            } else {
                str[i] = ' ';
            }
        }
    }
    str[lenout-1] = 0;
    return 0;
}
