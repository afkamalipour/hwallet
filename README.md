# HWallet

## Build

`make` to build, `make run` to build and start the host application.

## Hardware Setup

### Required components
* [FRDM-K82F](https://www.nxp.com/products/processors-and-microcontrollers/arm-based-processors-and-mcus/kinetis-cortex-m-mcus/k-seriesperformancem4/k8x-secure/freedom-development-platform-for-kinetis-k82-k81-and-k80-mcus:FRDM-K82F) or [FRDM-KL82Z](https://www.nxp.com/support/developer-resources/evaluation-and-development-boards/freedom-development-boards/mcu-boards/freedom-development-board-for-kinetis-ultra-low-power-kl82-mcus:FRDM-KL82Z)

* [Pmod OLED](https://reference.digilentinc.com/reference/pmod/pmodoled/start)

### Wiring

K82 | OLED | KL82
:---:|:---:|:---:
PORT B.18|Reset|PORT C.12
PORT B.19|D/C#|PORT C.16
PORT B.20|CS|PORT D.4
PORT B.21|SCLK|PORT D.5
PORT B.22|MOSI|PORT D.6
+5V|VBATC|+5V
+3V3|VDDC|+3V3
+5V|VCC|+5V
GND|GND|GND
