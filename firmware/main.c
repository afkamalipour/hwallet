//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include <stdint.h>
#include "common/clock.h"
#include "common/port.h"
#include "common/gpio.h"
#include "common/uart.h"
#include "common/packet.h"
#include "common/oled.h"
#include "common/crypto.h"

#include "modules/bitcoin.h"

void init_clocks() {
    CLOCK_Init();
    CLOCK_SourceUART(2);
    CLOCK_Enable(CLOCK_PORTA);
    CLOCK_Enable(CLOCK_PORTB);
    CLOCK_Enable(CLOCK_PORTC);
    CLOCK_Enable(CLOCK_PORTD);
}

void init_pins() {
    #ifdef KL82

    // Blue LED
    PORT_PinMux(PORT_C, 0, 1);
    GPIO_PinInit(GPIO_C, GPIO_OUTPUT(0, 0));
    // Buttons
    PORT_PinMux(PORT_D, 0, 1);
    GPIO_PinInit(GPIO_D, GPIO_INPUT(0));
    PORT_PinMux(PORT_A, 4, 1);
    GPIO_PinInit(GPIO_A, GPIO_INPUT(4));
    // UART0 RX and TX
    PORT_PinMux(PORT_B, 16, 3);
    PORT_PinMux(PORT_B, 17, 3);
    // SPI_1
    PORT_PinMux(PORT_D, 4, 7);
    PORT_PinMux(PORT_D, 5, 7);
    PORT_PinMux(PORT_D, 6, 7);
    // DC# and Reset gpios
    PORT_PinMux(PORT_C, 12, 1);
    PORT_PinMux(PORT_C, 16, 1);

    #else

    // Blue LED
    PORT_PinMux(PORT_C, 10, 1);
    GPIO_PinInit(GPIO_C, GPIO_OUTPUT(10, 0));
    // Buttons
    PORT_PinMux(PORT_C, 6, 1);
    GPIO_PinInit(GPIO_C, GPIO_INPUT(6));
    PORT_PinMux(PORT_A, 4, 1);
    GPIO_PinInit(GPIO_A, GPIO_INPUT(4));
    // UART4 RX and TX
    PORT_PinMux(PORT_C, 14, 3);
    PORT_PinMux(PORT_C, 15, 3);
    // SPI2
    PORT_PinMux(PORT_B, 20, 2);
    PORT_PinMux(PORT_B, 21, 2);
    PORT_PinMux(PORT_B, 22, 2);
    // DC# and Reset gpios
    PORT_PinMux(PORT_B, 18, 1);
    PORT_PinMux(PORT_B, 19, 1);

    #endif
}

int main() {
    init_clocks();
    init_pins();
    CRYPTO_Init();
    PACKET_Init();
    #ifdef KL82
    OLED_Init(SPI_1, GPIO_C, 12, GPIO_C, 16);
    Bitcoin_Init(GPIO_D, 0, GPIO_A, 4);
    #else
    OLED_Init(SPI_2, GPIO_B, 19, GPIO_B, 18);
    Bitcoin_Init(GPIO_C, 6, GPIO_A, 4);
    #endif
    OLED_ShowHomeScreen();

    while(1) {
        Packet msg;
        PACKET_Receive(&msg);
        switch(PACKET_MODULE(msg.type)) {
            case PACKET_BITCOIN:
                Bitcoin_Process(&msg);
                break;
            default:
                PACKET_SendNAK();
        };
    }
    return 0;
}
