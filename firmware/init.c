//******************************************************************************
// https://gitlab.com/nemanjan/hwallet
// Copyright (C) 2018  Nemanja Nikodijevic <nemanja@hacke.rs>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//******************************************************************************

#include <stdint.h>

__attribute__ ((used,section(".flash_config"))) const struct {
    uint32_t KEY[2];
    uint8_t FPROT[4];
    uint8_t RESERVED[2];
    uint8_t FOPT;
    uint8_t FSEC;
} Flash_Config = {{0xFFFFFFFF, 0xFFFFFFFF}, {0xFF, 0xFF, 0xFF, 0xFF},
                  {0xFE, 0x3D}, 0xFF, 0xFF};

void Reset_Handler(void);
void Default_Handler(void);
extern void _stackTop(void);

__attribute__ ((used, section(".vectors")))
void (* const pVectors[])(void) = {
    &_stackTop,
    Reset_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    0,
    0,
    0,
    0,
    Default_Handler,
    Default_Handler,
    0,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
    Default_Handler,
};

extern int main(void);
extern uint32_t _data;
extern uint32_t _edata;
extern uint32_t _bss;
extern uint32_t _ebss;
extern uint32_t _data_loadaddr;

typedef struct {
    volatile uint16_t STCTRLH;
    volatile uint16_t STCTRLL;
    volatile uint16_t TOVALH;
    volatile uint16_t TOVALL;
    volatile uint16_t WINH;
    volatile uint16_t WINL;
    volatile uint16_t REFRESH;
    volatile uint16_t UNLOCK;
    volatile uint16_t TMROUTH;
    volatile uint16_t TMROUTL;
    volatile uint16_t RSTCNT;
    volatile uint16_t PRESC;
} _WDOG;

#define WDOG    ((_WDOG*)0x40052000)

void Reset_Handler(void) {
    __asm volatile ("cpsid i");

    // Unlock and disable watchdog
    WDOG->UNLOCK = 0xC520;
    WDOG->UNLOCK = 0xD928;
    WDOG->STCTRLH = 0x01D2u;

    uint32_t *ptr = (uint32_t*)&_data_loadaddr;
    for (uint32_t *i = (uint32_t*)&_data; i < (uint32_t*)&_edata; i++)
        *i = *(ptr++);
    for (uint32_t *i = (uint32_t*)&_bss; i < (uint32_t*)&_ebss; i++)
        *i = 0;
    __asm volatile ("cpsie i");
    main();
    while(1);
}

void Default_Handler(void) {
    while(1);
}
