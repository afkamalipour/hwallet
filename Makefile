TARGET := K82
CC	:=	arm-none-eabi-gcc
OBJCOPY := arm-none-eabi-objcopy

KCCOPT := -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -Wall
KLDOPT := -T scripts/k82fn256xxx15.ld -nostdlib
KDEVICE := MK82FN256XXX15

KLCCOPT := -mcpu=cortex-m0plus -mthumb -Wall -DKL82
KLLDOPT := -T scripts/kl82z128xxx7.ld -nostdlib
LDEVICE := MKL82Z128XXX7

ifeq ($(TARGET), K82)
	CCOPT := $(KCCOPT)
	LDOPT := $(KLDOPT)
	DEVICE := $(KDEVICE)
endif
ifeq ($(TARGET), KL82)
	CCOPT := $(KLCCOPT)
	LDOPT := $(KLLDOPT)
	DEVICE := $(LDEVICE)
endif

FW_SRC := firmware/main.c\
		  firmware/init.c\
		  common/clock.c\
		  common/gpio.c\
		  common/uart.c\
		  common/packet.c\
		  common/spi.c\
		  common/oled_font.c\
		  common/oled.c\
		  common/crypto.c\
		  common/utils.c\
		  common/flash.c\
		  modules/bitcoin.c

OUTDIR := output
RUNSCRIPT := $(OUTDIR)/run.jlink

.PHONY: firmware run clean
default: all

all: firmware

firmware:
	mkdir -p $(OUTDIR)
	$(CC) $(CCOPT) $(LDOPT) $(FW_SRC) -I. -o $(OUTDIR)/firmware.axf
	$(OBJCOPY) -O binary $(OUTDIR)/firmware.axf $(OUTDIR)/firmware.bin

run: firmware
	echo "h\nerase\nloadbin ${OUTDIR}/firmware.bin, 0\nr\ng\nq\n" > $(RUNSCRIPT)
	JLinkExe -if swd -device $(DEVICE) -speed 4000 -CommanderScript $(RUNSCRIPT)
	python3 ./app/comm.py

clean:
	rm -rf $(OUTDIR)
